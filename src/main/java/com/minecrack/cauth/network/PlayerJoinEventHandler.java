package com.minecrack.cauth.network;

import com.google.common.eventbus.Subscribe;
import com.minecrack.cauth.CAuthContainer;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.common.network.FMLNetworkEvent;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.network.NetHandlerPlayServer;
import net.minecraft.network.Packet;

public class PlayerJoinEventHandler {

    @SubscribeEvent
    public void onPlayerJoin(FMLNetworkEvent.ServerConnectionFromClientEvent event) {
        if(event.handler instanceof NetHandlerPlayServer) {
            EntityPlayerMP player = ((NetHandlerPlayServer)event.handler).playerEntity;
            String username = player.getGameProfile().getName();

            String skinURL = CAuthContainer.proxy.getSkinConfig(username);
            String cloakURL = CAuthContainer.proxy.getCloakConfig(username);
            //String skinURL = CAuthContainer.skins.get(username);
            //String cloakURL = CAuthContainer.skins.get(username);

            Packet packet = CAuthContainer.network.getPacketFrom(new TextureMessage(skinURL, cloakURL));
            ((NetHandlerPlayServer)event.handler).sendPacket(packet);
        }
    }
}
