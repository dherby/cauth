package com.minecrack.cauth.network;

import com.minecrack.cauth.CAuthContainer;
import com.minecrack.cauth.proxy.ClientProxy;
import cpw.mods.fml.common.network.simpleimpl.IMessage;
import cpw.mods.fml.common.network.simpleimpl.IMessageHandler;
import cpw.mods.fml.common.network.simpleimpl.MessageContext;
import net.minecraft.client.Minecraft;

public class TextureMessageHandler implements IMessageHandler<TextureMessage, IMessage> {

    @Override
    public IMessage onMessage(TextureMessage message, MessageContext ctx) {
        String username = Minecraft.getMinecraft().getSession().getUsername();
        if(message.skinURL != null)
            ((ClientProxy)CAuthContainer.proxy).skinURL = message.skinURL;
            //CAuthContainer.skins.put(username, message.skinURL);
        if(message.cloakURL != null)
            ((ClientProxy)CAuthContainer.proxy).cloakURL = message.cloakURL;
            //CAuthContainer.cloaks.put(username, message.cloakURL);
        return null;
    }
}
