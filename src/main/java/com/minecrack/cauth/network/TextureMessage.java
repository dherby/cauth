package com.minecrack.cauth.network;

import cpw.mods.fml.common.network.ByteBufUtils;
import cpw.mods.fml.common.network.simpleimpl.IMessage;
import io.netty.buffer.ByteBuf;

public class TextureMessage implements IMessage {

    private byte isSkin = 1;
    private byte isCloak = 1;
    public String skinURL = null;
    public String cloakURL = null;

    public TextureMessage() { }

    TextureMessage(String skin, String cloak) {
        skinURL = skin;
        cloakURL = cloak;

        if(skinURL == null) isSkin = 0;
        if(cloakURL == null) isCloak = 0;
    }

    @Override
    public void fromBytes(ByteBuf buf) {
        isSkin = buf.readByte();
        isCloak = buf.readByte();

        if(isSkin == 1) skinURL = ByteBufUtils.readUTF8String(buf);
        if(isCloak == 1)cloakURL = ByteBufUtils.readUTF8String(buf);
    }

    @Override
    public void toBytes(ByteBuf buf) {
        buf.writeByte(isSkin);
        buf.writeByte(isCloak);
        if(isSkin == 1) ByteBufUtils.writeUTF8String(buf, skinURL);
        if(isCloak == 1) ByteBufUtils.writeUTF8String(buf, cloakURL);
    }
}
