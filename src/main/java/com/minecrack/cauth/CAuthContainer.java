package com.minecrack.cauth;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.minecrack.cauth.network.*;
import com.minecrack.cauth.proxy.ClientProxy;
import com.minecrack.cauth.proxy.CommonProxy;
import com.minecrack.cauth.proxy.ServerProxy;
import cpw.mods.fml.common.DummyModContainer;
import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.LoadController;
import cpw.mods.fml.common.ModMetadata;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.network.NetworkRegistry;
import cpw.mods.fml.common.network.simpleimpl.SimpleNetworkWrapper;
import cpw.mods.fml.relauncher.Side;
import net.minecraft.server.MinecraftServer;

import java.util.Arrays;
import java.util.HashMap;

public class CAuthContainer  extends DummyModContainer {

    public static CommonProxy proxy;

    //public static HashMap<String, String> skins = new HashMap();
    //public static HashMap<String, String> cloaks = new HashMap();

    public static SimpleNetworkWrapper network;

    public CAuthContainer() {
        super(new ModMetadata());
        ModMetadata myMeta = super.getMetadata();
        myMeta.authorList = Arrays.asList("Unknown");
        myMeta.description = "A Custom authentication mod";
        myMeta.modId = "CAuth";
        myMeta.version = "1.7.10";
        myMeta.name = "Custom Authentication";
        myMeta.url = "http://google.com/";
    }

    @Override
    public boolean registerBus(EventBus bus, LoadController controller) {
        bus.register(this);
        return true;
    }

    @Subscribe
    public void onPreInit(FMLPreInitializationEvent event)
    {
        proxy = FMLCommonHandler.instance().getEffectiveSide().isClient() ? new ClientProxy() : new ServerProxy();
        proxy.injectPatch();
        proxy.loadConfig(event.getSuggestedConfigurationFile());

        FMLCommonHandler.instance().bus().register(new PlayerJoinEventHandler());

        network = NetworkRegistry.INSTANCE.newSimpleChannel("CAuth");
        network.registerMessage(TextureMessageHandler.class, TextureMessage.class, 0, Side.CLIENT);
    }
}
