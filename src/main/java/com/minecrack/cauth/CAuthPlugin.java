package com.minecrack.cauth;

import com.minecrack.cauth.transformers.TransformerPatchClass;
import cpw.mods.fml.relauncher.IFMLLoadingPlugin;

import java.util.Map;
import java.util.logging.Logger;

@IFMLLoadingPlugin.TransformerExclusions({"com.minecrack.cauth"})
@IFMLLoadingPlugin.MCVersion("1.7.10")
public class CAuthPlugin implements IFMLLoadingPlugin {

    public static final Logger logger = Logger.getLogger("Minecraft");

    @Override
    public String[] getASMTransformerClass() {
        return new String[] {TransformerPatchClass.class.getName()};
    }

    @Override
    public String getModContainerClass() {
        return CAuthContainer.class.getName();
    }

    @Override
    public void injectData(Map<String, Object> data) {}

    @Override
    public String getSetupClass() { return null; }

    @Override
    public String getAccessTransformerClass() { return null; }
}
