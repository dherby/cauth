package com.minecrack.cauth.authentication;

import com.mojang.authlib.Agent;
import com.mojang.authlib.yggdrasil.YggdrasilAuthenticationService;
import com.mojang.authlib.yggdrasil.YggdrasilUserAuthentication;

public class CAuthUserAuthentication extends YggdrasilUserAuthentication {
    public CAuthUserAuthentication(YggdrasilAuthenticationService service, Agent agent) {
        super(service, agent);
    }
}
