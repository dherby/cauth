package com.minecrack.cauth.authentication;

import com.mojang.authlib.yggdrasil.YggdrasilAuthenticationService;
import com.mojang.authlib.yggdrasil.YggdrasilGameProfileRepository;

public class CAuthGameProfileRepository extends YggdrasilGameProfileRepository {

    public CAuthGameProfileRepository(YggdrasilAuthenticationService authenticationService) {
        super(authenticationService);
    }
}
