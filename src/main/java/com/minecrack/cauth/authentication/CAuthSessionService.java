package com.minecrack.cauth.authentication;

import com.google.common.base.Charsets;
import com.minecrack.cauth.CAuthContainer;
import com.mojang.authlib.GameProfile;
import com.mojang.authlib.exceptions.AuthenticationException;
import com.mojang.authlib.exceptions.AuthenticationUnavailableException;
import com.mojang.authlib.minecraft.MinecraftProfileTexture;
import com.mojang.authlib.minecraft.MinecraftSessionService;
import com.mojang.authlib.yggdrasil.YggdrasilAuthenticationService;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class CAuthSessionService implements MinecraftSessionService {

    public CAuthSessionService(YggdrasilAuthenticationService authenticationService) {
    }

    @Override
    public GameProfile fillProfileProperties(GameProfile gameProfile, boolean requireSecure) {
        throw new RuntimeException();
        //return null;
    }

    @Override
    public void joinServer(GameProfile profile, String authenticationToken, String serverId) throws AuthenticationException {
    }

    @Override
    public Map<MinecraftProfileTexture.Type, MinecraftProfileTexture> getTextures(GameProfile profile, boolean requireSecure) {
        /*
        Property textureProperty = (Property) Iterables.getFirst(profile.getProperties().get("textures"), null);
        if (textureProperty == null) {
            return new HashMap();
        }
        return null;
        */
        HashMap<MinecraftProfileTexture.Type, MinecraftProfileTexture> result = new HashMap();

        result.put(MinecraftProfileTexture.Type.SKIN, new MinecraftProfileTexture(CAuthContainer.proxy.getSkin()));
        result.put(MinecraftProfileTexture.Type.CAPE, new MinecraftProfileTexture(CAuthContainer.proxy.getCloak()));

        return result;
    }

    @Override
    public GameProfile hasJoinedServer(GameProfile user, String serverId) throws AuthenticationUnavailableException {
        UUID newid = UUID.nameUUIDFromBytes(("OfflinePlayer:" + user.getName()).getBytes(Charsets.UTF_8));
        GameProfile result = new GameProfile(newid, user.getName());
        return result;
    }
}
