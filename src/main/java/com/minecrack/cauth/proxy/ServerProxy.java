package com.minecrack.cauth.proxy;

import com.minecrack.cauth.CAuthContainer;
import com.minecrack.cauth.authentication.CAuthGameProfileRepository;
import com.minecrack.cauth.authentication.CAuthSessionService;
import net.minecraft.server.MinecraftServer;
import net.minecraftforge.common.config.Configuration;

import java.io.File;
import java.lang.reflect.Field;

public class ServerProxy extends CommonProxy {

    @Override
    public void injectPatch() {
        MinecraftServer server = MinecraftServer.getServer();

        try {
            Field fieldAuthSession = server.getClass().getSuperclass().getDeclaredField("field_147143_S");
            Field fieldProfileRepository = server.getClass().getSuperclass().getDeclaredField("field_152365_W");

            setPrivateField(fieldAuthSession, server, new CAuthSessionService(null));
            setPrivateField(fieldProfileRepository, server, new CAuthGameProfileRepository(null));

        } catch(Exception e) {
            e.printStackTrace();
        }
    }


}
