package com.minecrack.cauth.proxy;

import com.minecrack.cauth.CAuthContainer;
import net.minecraftforge.common.config.Configuration;

import java.io.File;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;

public abstract class CommonProxy {

    private Configuration config;

    public abstract void injectPatch();

    public boolean isSinglePlayer() {
        return false;
    }

    public void loadConfig(File file) {
        config = new Configuration(file);
    }

    public String getSkin() { return null; }
    public String getCloak() { return null; }

    public String getSkinConfig(String username) {
        config.load();
        String skinURL = config.get("skin", username, "null").getString();
        config.save();

        if(skinURL.equals("null")) {
            return null;
        } else {
            return skinURL;
        }
    }

    public String getCloakConfig(String username) {
        config.load();
        String cloakURL = config.get("cloak", username, "null").getString();
        config.save();

        if(cloakURL.equals("null")) {
            return null;
        } else {
            return cloakURL;
        }
    }

    protected void setPrivateField(Field field, Object instance, Object value) throws Exception {
        field.setAccessible(true);
        Field modifiersField = Field.class.getDeclaredField("modifiers");
        modifiersField.setAccessible(true);
        modifiersField.setInt(field, field.getModifiers() & ~Modifier.FINAL);

        field.set(instance, value);
    }
}
