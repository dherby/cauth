package com.minecrack.cauth.proxy;

import com.minecrack.cauth.CAuthContainer;
import net.minecraft.client.Minecraft;
import net.minecraftforge.common.config.Configuration;

import java.io.File;

public class ClientProxy extends CommonProxy {

    public String skinURL = null;
    public String cloakURL = null;
    @Override
    public boolean isSinglePlayer() {
        return Minecraft.getMinecraft().isSingleplayer();
    }

    @Override
    public void injectPatch() {

    }

    @Override
    public String getSkin() { return skinURL; }

    @Override
    public String getCloak() { return cloakURL; }
}
