package com.minecrack.cauth.transformers;

import cpw.mods.fml.common.asm.transformers.AccessTransformer;
import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassWriter;

import java.io.IOException;

public class TransformerPatchClass extends AccessTransformer {

    public static final String patch = "com.mojang.authlib.yggdrasil.YggdrasilAuthenticationService";

    public TransformerPatchClass() throws IOException {
        super();
    }

    @Override
    public byte[] transform(String name, String transformedName, byte[] bytes) {
        if(patch.equals(name)) {
            try {
                ClassReader cr = new ClassReader(bytes);
                ClassWriter cw = new ClassWriter(cr, 0);
                CAuthClassVisitor ca = new CAuthClassVisitor(cw);
                cr.accept(ca, 0);
                return cw.toByteArray();
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
        return bytes;
    }
}
