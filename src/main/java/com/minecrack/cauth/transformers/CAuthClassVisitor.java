package com.minecrack.cauth.transformers;

import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;

public class CAuthClassVisitor extends ClassVisitor {

    public CAuthClassVisitor(ClassVisitor cv)
    {
        super(Opcodes.ASM4, cv);
    }

    @Override
    public MethodVisitor visitMethod(int access, String name, String desc, String signature, String[] exceptions)
    {
        MethodVisitor mv = cv.visitMethod(access, name, desc, signature, exceptions);
        if(mv != null)
        {
            mv = new CAuthMethodVisitor(mv);
        }
        return mv;
    }
}
