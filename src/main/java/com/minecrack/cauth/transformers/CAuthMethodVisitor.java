package com.minecrack.cauth.transformers;

import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;

public class CAuthMethodVisitor extends MethodVisitor {

    public static final String base_package_to_patch = "com/mojang/authlib/yggdrasil/";
    public static final String base_package_patched = "com/minecrack/cauth/authentication/";

    public static final String class_to_patch[] = {
            "YggdrasilMinecraftSessionService",
            "YggdrasilUserAuthentication",
            "YggdrasilGameProfileRepository"
    };

    public static final String class_patched[] = {
            "CAuthSessionService",
            "CAuthUserAuthentication",
            "CAuthGameProfileRepository"
    };

    public CAuthMethodVisitor(MethodVisitor mv)
    {
        super(Opcodes.ASM4, mv);
    }

    @Override
    public void visitTypeInsn(int opcode, String type) {
        if (mv != null) {
            if(opcode == Opcodes.NEW) {
                visitNew(type);
            } else {
                mv.visitTypeInsn(opcode, type);
            }
        }
    }

    @Override
    public void visitMethodInsn(int opcode, String owner, String name, String desc) {
        if (mv != null) {
            if(opcode == Opcodes.INVOKESPECIAL) {
                visitInvoke(owner, name, desc);
            } else {
                mv.visitMethodInsn(opcode, owner, name, desc);
            }
        }
    }

    private void visitNew(String type) {
        for(int i = 0; i < class_to_patch.length; i++) {
            if(type.equals(base_package_to_patch + class_to_patch[i])) {
                mv.visitTypeInsn(Opcodes.NEW, base_package_patched + class_patched[i]);
                return;
            }
        }
        mv.visitTypeInsn(Opcodes.NEW, type);
    }

    private void visitInvoke(String owner, String name, String desc) {
        for(int i = 0; i < class_to_patch.length; i++) {
            if(owner.equals(base_package_to_patch + class_to_patch[i])) {
                mv.visitMethodInsn(Opcodes.INVOKESPECIAL, base_package_patched + class_patched[i], name, desc);
                return;
            }
        }
        mv.visitMethodInsn(Opcodes.INVOKESPECIAL, owner, name, desc);
    }
}
